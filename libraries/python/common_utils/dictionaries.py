"""
Provides common quality of life functions for dictionaries.
"""
from typing import List, Dict, Any


__all__: List[str] = [
    'default_if_none'
]


def default_if_none(dictionary: Dict[Any, Any], key: Any, default: Any) -> Any:
    """
    Returns the value of a dictionary key if it exists, otherwise returns the default value.
    input:
        dictionary: Dictionary to get value from
        key: Key to get value from
        default: Default value to return if key does not exist
    output:
        Value of key if it exists, otherwise default value
    """
    value = dictionary.get(key, default)
    if value is None:
        return default
    return value
