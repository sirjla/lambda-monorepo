"""
Date utilities
"""

from typing import List, Dict, Any, Union, Callable
import datetime as dt


__all__: List[str] = [
    'get_interval_string',
    'extract_format_timestamp',
    'date_comparer',
    'get_yesterday',
]


def get_interval_string() -> str:
    """ Gets an ISO-8601 interval from now for the last 2 days """
    now = dt.datetime.now().replace(microsecond=0)
    previous_date = (now - dt.timedelta(hours=36)).replace(microsecond=0)
    return f"{ previous_date.isoformat() }/{ now.isoformat() }"


def extract_format_timestamp(row: Dict[str, Any], key: str) -> Union[str, None]:
    """ Extracts a date from a string in the format YYYY-MM-DD """
    try:
        return row[key].strftime('%Y-%m-%d %H:%M:%S') if row[key] is not None else None
    except TypeError:
        return None


def date_comparer(
        first_date: Union[None, dt.datetime],
        second_date: Union[None, dt.datetime],
        operation_function: Callable
    ) -> Union[None, dt.datetime]:
    """
    Compares two dates using the operation_function.
    """
    if first_date is None and second_date is None:
        return None
    if first_date is None:
        return second_date
    if second_date is None:
        return first_date
    return operation_function(first_date, second_date)


def get_yesterday() -> dt.date:
    """
    Returns yesterday's date
    """
    return dt.date.today() - dt.timedelta(days=1)
