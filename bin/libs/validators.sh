function check_dependency() {
  local readonly program=${1}
  type ${program} > /dev/null 2>&1 || {
    echo >&2 "${program} is not installed. Aborting."
    exit 1
  }
}

function check_dependencies() {
  for var in "${@}"
  do
      check_dependency "${var}"
  done
}

function validate_value_regex() {
    local readonly value=${1}
    local readonly regex=${2}
    if [[ ! ${value} =~ ${regex} ]]
    then
        echo "Invalid value: ${value} does not comply with ${regex}"
        exit 1
    fi
}
