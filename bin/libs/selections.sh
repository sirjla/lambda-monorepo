function select_architecture() {
  echo "Selecting architecture:"
  local readonly architectures=('arm64' 'x86_64')
  select opt in "${architectures[@]}"; do 
    if [ $REPLY -gt $((${#architectures[@]})) -o $REPLY -lt 1 ]
    then
      echo "Invalid option. Try another one."
    else
      architecture="${opt}"
      break
    fi
  done
}

function select_runtime() {
  echo 'Select runtime:'
  local readonly runtimes=('Python')
  select opt in "${runtimes[@]}"; do 
    case "$REPLY" in
      1) runtime='python';return;;
      *) echo "Invalid option. Try another one.";continue;;
    esac
  done
}

function select_version() {
  echo 'Select version:'
  local readonly python_versions=('3.9')
  case "${runtime}" in
    python) select opt in "${python_versions[@]}"; do 
      if [ $REPLY -gt $((${#python_versions[@]})) -o $REPLY -lt 1 ]
      then
        echo "Invalid option. Try another one."
      else
        version="${opt}"
        break
      fi
    done;;
  esac
}

function select_layer_build() {
  echo 'Select layer build style:'
  local readonly layer_build_styles=('Zip' 'S3')
  select opt in "${layer_build_styles[@]}"; do 
    case "$REPLY" in
      1) layer_build="zip";return;;
      2) layer_build="s3";return;;
      *) echo "Invalid option. Try another one.";continue;;
    esac
  done
}

function multiselect_menu() {
  local msg=''
  local readonly options=("${@}")
  echo "Avaliable options:"
  for i in ${!options[@]}; do
      printf "%3d%s) %s\n" $((i+1)) "${__choices[i]:- }" "${options[i]}"
  done
  [[ "$msg" ]] && echo "$msg"; :
}
 
function multiselect() {
  local readonly inputs=("${@}")
  local __result=${inputs[0]}
  local readonly options=("${inputs[@]:1}")
  if [[ ${#options[@]} -eq 0 ]]
  then
    return 0
  fi
  declare __choices=()
  for i in ${!options[@]}; do
      __choices[i]=''
  done
  local selected=()
  prompt="Check an option (again to uncheck, ENTER when done): "
  while multiselect_menu "${options[@]}" && read -rp "$prompt" num && [[ "$num" ]]; do
      [[ "$num" != *[![:digit:]]* ]] &&
      (( num > 0 && num <= ${#options[@]} )) ||
      { msg="Invalid option: $num"; continue; }
      ((num--)); msg="${options[num]} was ${__choices[num]:+un}checked"
      [[ "${__choices[num]}" ]] && __choices[num]="" || __choices[num]="+"
  done
  
  for i in ${!options[@]}; do
      [[ "${__choices[i]}" ]] && selected+=("${options[$i]}")
  done
  if [[ ${#selected[@]} -gt 0 ]]
  then
    eval $__result="(${selected[@]})"
  fi
}
