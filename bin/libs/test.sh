 ##
# Multi-select menu in bash script
#
# bash-only code that does exactly what you want. It's short (~20 lines), but a
# bit cryptic for a begginner. Besides showing "+" for checked options, it also
# provides feedback for each user action ("invalid option", "option X was
# checked"/unchecked, etc). -- http://serverfault.com/a/298312
##
 
# customize with your own.
optionss=("AAA" "BBB" "CCC" "DDD")
 
function multiselect_menu() {
    local readonly options=("${@}")
    echo "Avaliable options:"
    for i in ${!options[@]}; do
        printf "%3d%s) %s\n" $((i+1)) "${__choices[i]:- }" "${options[i]}"
    done
    [[ "$msg" ]] && echo "$msg"; :
}
 
function multiselect() {
    local readonly inputs=("${@}")
    local __result=${inputs[0]}
    local readonly options=("${inputs[@]:1}")
    local selected=()
    prompt="Check an option (again to uncheck, ENTER when done): "
    while multiselect_menu "${options[@]}" && read -rp "$prompt" num && [[ "$num" ]]; do
        [[ "$num" != *[![:digit:]]* ]] &&
        (( num > 0 && num <= ${#options[@]} )) ||
        { msg="Invalid option: $num"; continue; }
        ((num--)); msg="${options[num]} was ${__choices[num]:+un}checked"
        [[ "${__choices[num]}" ]] && __choices[num]="" || __choices[num]="+"
    done
    
    for i in ${!options[@]}; do
        [[ "${__choices[i]}" ]] && selected+=("${options[$i]}")
    done
    eval $__result="(${selected[@]})"
}
lala=()
multiselect lala $(yq '.libraries[] | select(.runtime == "python").name' config.yaml)
echo "${lala[@]}"
