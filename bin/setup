# Execution settings ========================================================== #
set -o errexit   # abort on nonzero exitstatus
set -o nounset   # abort on unbound variable
set -o pipefail  # don't hide errors within pipes
# ============================================================================= #


# Imports ===================================================================== #
source "bin/libs/validators.sh"
source "bin/libs/selections.sh"
# ============================================================================= #

# Global Variables ============================================================ #
declare action=""
declare runtime=""
declare version=""
declare architecture=""
declare create_name=""
declare -a libraries=()
declare -a layers=()
declare layer_build=''
declare layer_s3_url=''
# ============================================================================= #


# Functions =================================================================== #
function help() {
    echo "Usage: $0 action"
    echo "  action   : Name of the action to invoke,"
    echo "             can be one of the following:"
    echo "             - init"
    echo "               Initializes the local environment."
    echo "             - pull"
    echo "               Pulls the layers and installs deps to the local environment."
    echo "             - function"
    echo "               Creates a new function."
    echo "             - library"
    echo "               Creates a new library."
    echo "             - layer"
    echo "               Creates a new layer."
    echo "  -h       : Show this help."
    exit 1
}

function get_variables() {
if [[ $# -eq 0 ]]
  then
    echo "No action supplied"
    help
fi
action="${1}"
while getopts h flag
do
  case "${flag}" in
    h) help;;
  esac
done
}

function init() {
  echo "Initializing local environment..."
  mkdir -p tmp
  echo "Done."
}

function pull() {
  local runtime=''
  local version=''
  local build_type=''
  local s3_url=''
  echo "Pulling layers to the local environment..."
  rm -rf local_layers
  for layer in $(yq '.layers[].name' config.yaml)
  do
    echo "Pulling ${layer}..."
    runtime=$(yq ".layers[] | select(.name == \"${layer}\") | .runtime" config.yaml)
    version=$(yq ".layers[] | select(.name == \"${layer}\") | .version" config.yaml)
    build_type=$(yq ".build_type" "layers/${runtime}/${version}/${layer}/build.yaml")
    architecture=$(yq ".architecture" "layers/${runtime}/${version}/${layer}/build.yaml")
    s3_url=$(yq ".s3_url" "layers/${runtime}/${version}/${layer}/build.yaml")
    mkdir -p "local_layers/${architecture}/${runtime}/${version}"
    if [[ ${build_type} == 's3' ]]
    then
      aws s3 cp "${s3_url}" /tmp/tmp.zip
      unzip -qq /tmp/tmp.zip -d "local_layers/${architecture}/${runtime}/${version}"
      rm /tmp/tmp.zip
    fi
    if [[ ${build_type} == 'zip' ]]
    then
      if [[ $(aws s3 ls "s3://bucket/lambda/layers/${layer}.zip") ]]
      then
        aws s3 cp "s3://bucket/lambda/layers/${layer}.zip" /tmp/tmp.zip
        unzip -qq /tmp/tmp.zip -d "local_layers/${architecture}/${runtime}/${version}"
        rm /tmp/tmp.zip
      else
        echo "Layer ${layer} is yet to be built."
      fi
    fi
  done
  echo "Installing requirements"
  rm -rf local_libs
  for req in $(find ./libraries/python -type f -name "requirements.txt")
  do
    pip install -r "${req}" -t local_libs/python
  done
  for req in $(find ./functions/python -type f -name "requirements.txt")
  do
    pip install -r "${req}" -t local_libs/python
  done
  echo "Done."
}

function gather_creation_variables() {
  # Gathers the variables needed to create a new function.
  # ${1} must be a regex
  # ${2} must be a key from config.yaml
  # ${3} must be a string (rval) that specifies the selects to use:
  #   - 'r' for runtime
  #   - 'v' for version
  #   - 'a' for architecture
  #   - 'l' for layer
  # e.g. gather_creation_variables '^[a-zA-Z0-9\-_]+$' 'functions' 'rva'
  #  - will ask for the runtime, version and architecture
  # e.g. gather_creation_variables '^[a-zA-Z0-9\-_]+$' 'functions' 'r'
  #  - will ask for the runtime
  local readonly regexp="${1}"
  local readonly yaml_key="${2}"
  local readonly selects="${3}"
  read -r -p "Enter name (must comply with regexp ${regexp}: "
  validate_value_regex "${REPLY}" "${regexp}"
  create_name="${REPLY}"
    if [[ $(yq ".${yaml_key}[] | select(.name == \"${create_name}\")" config.yaml | wc -l)  -ge 1 ]]
  then
    echo "${create_name} already exists."
    exit 1
  fi
  if [[ "${selects}" == *"r"* ]]
  then
    select_runtime
  fi
  if [[ "${selects}" == *"v"* ]]
  then
    select_version
  fi
  if [[ "${selects}" == *"a"* ]]
  then
    select_architecture
  fi
  if [[ "${selects}" == *"l"* ]]
  then
    select_layer_build
    if [[ "${layer_build}" == "s3" ]]
    then
      read -r -p "Enter path (must comply with regexp ^s3:\/\/[a-z0-9_-]+\/.*\.zip$): "
      validate_value_regex "${REPLY}" '^s3:\/\/[a-z0-9_-]+\/.*\.zip$'
      layer_s3_url="${REPLY}"
    fi
  fi
}

function python_base_files() {
  echo """import json\n\n\ndef handler(event, context):
    return {
        'statusCode': 200,
        'body': json.dumps({
            'message': 'Hello World!'
        })
    }""" > "functions/${runtime}/${create_name}/src/index.py"
}

function create_function_files() {
  mkdir -p "functions/${runtime}/${create_name}/src"
  mkdir -p "functions/${runtime}/${create_name}/events"
  if [[ "${runtime}" == "python" ]]
  then
    python_base_files
  fi
  echo "{}" > "functions/${runtime}/${create_name}/events/event.json"
  echo """language: ${runtime}
version: ${version}
architecture: ${architecture}
name: ${create_name}
libraries: []
layers: []""" > "functions/${runtime}/${create_name}/build.yaml"
  for library in ${libraries[@]+"${libraries[@]}"}
  do
    yq -i ".libraries += \"${library}\"" "functions/${runtime}/${create_name}/build.yaml"
  done
  for layer in ${layers[@]+"${layers[@]}"}
  do
    yq -i ".layers += \"${layer}\"" "functions/${runtime}/${create_name}/build.yaml"
  done
}

function create_library_files() {
  mkdir -p "libraries/${runtime}/${create_name}"
  if [[ "${runtime}" == "python" ]]
  then
    touch "libraries/${runtime}/${create_name}/__index__.py"
    touch "libraries/${runtime}/${create_name}/requirements.txt"
  fi
}

function create_layer_files() {
  mkdir -p "layers/${runtime}/${version}/${create_name}"
  echo """language: ${runtime}
version: ${version}
architecture: ${architecture}
name: ${create_name}
build_type: ${layer_build}""" > "layers/${runtime}/${version}/${create_name}/build.yaml"
  if [[ "${layer_build}" == "zip" ]]
  then
    echo 'Select libraries:'
    multiselect libraries $(yq ".libraries[] | select(.runtime == \"${runtime}\").name" config.yaml)
    yq -i '. | (.libraries = [])' "layers/${runtime}/${version}/${create_name}/build.yaml"
    yq -i '. | (.extra_scripts = [])' "layers/${runtime}/${version}/${create_name}/build.yaml"
    yq -i '. | (.extra_directories = [])' "layers/${runtime}/${version}/${create_name}/build.yaml"
    for library in ${libraries[@]+"${libraries[@]}"}
    do
      yq -i ".libraries += \"${library}\"" "layers/${runtime}/${version}/${create_name}/build.yaml"
    done
    if [[ "${runtime}" == "python" ]]
    then
      touch "layers/${runtime}/${version}/${create_name}/requirements.txt"
      echo "pip install -r requirements.txt -t python/lib/python3.9/site-packages/\nzip -r9 layer.zip python" > "layers/${runtime}/${version}/${create_name}/build.sh"
    fi
  elif [[ "${layer_build}" == "s3" ]]
  then
    yq -i ". | (.s3_url = \"${layer_s3_url}\")" "layers/${runtime}/${version}/${create_name}/build.yaml"
  fi
}

function new_function() {
  echo "Creating new function..."
  gather_creation_variables '^[A-Z][a-zA-Z0-9_-]+$' 'functions' 'rva'
  echo 'Select libraries:'
  multiselect libraries $(yq ".libraries[] | select(.runtime == \"${runtime}\").name" config.yaml)
  echo 'Select layers:'
  multiselect layers $(yq ".layers[] | select(.runtime == \"${runtime}\" and .version == ${version} and .architecture == \"${architecture}\").name" config.yaml)
  create_function_files
  yq -i ".functions += {\"name\": \"${create_name}\", \"runtime\": \"${runtime}\"}" config.yaml
  echo "Created ${create_name} function."
}

function new_library() {
  echo "Creating new library..."
  gather_creation_variables '^[a-zA-Z0-9_-]+$' 'libraries' 'r'
  create_library_files
  yq -i ".libraries += {\"name\": \"${create_name}\", \"runtime\": \"${runtime}\"}" config.yaml
  echo "Created library: ${create_name}."
}

function new_layer() {
  echo "Creating new layer..."
  gather_creation_variables '^[A-Z][a-zA-Z0-9_-]+$' 'layers' 'rval'
  create_layer_files
  yq -i ".layers += {\"name\": \"${create_name}\", \"runtime\": \"${runtime}\", \"version\": ${version}, \"architecture\": \"${architecture}\"}" config.yaml
  echo "Created layer: ${create_name}."
}

function main(){
    get_variables "${@+$@}"
    case "${action}" in
        init) init;;
        pull) pull;;
        function) new_function;;
        library) new_library;;
        layer) new_layer;;
        *) echo "Unknown action: ${action}"
           help;;
    esac
}
# ============================================================================= #


# Terminate the script if the neccesary dependencies are not found ============ #
check_dependencies yq aws unzip pip
# ============================================================================= #


# Run the script ============================================================== #
main "${@:+$@}"
# ============================================================================= #
