import org.jenkinsci.plugins.pipeline.modeldefinition.Utils

def config
pipeline {
  agent {
    label 'python-3.9-arm64'
  }
  stages {
    stage('Load Config') {
      steps {
        script {
          config = readYaml file: 'config.yaml'
        }
      }
    }
    stage('Lint') {
      stages {
        stage('Setup') {
          steps {
            sh 'mkdir -p lint'
            sh 'pip install pylint'
          }
        }
        stage('Libraries') {
          steps {
            echo 'Libraries'
            script {
              config['libraries'].each{ lib ->
                if (lib['runtime'] == 'python') {
                  sh "pylint --disable=E0401 --exit-zero --output-format=parseable --reports=no 'libraries/python/${lib['name']}' > 'lint/${lib['name']}.pylint.log'"
                }
              }
            }
          }
        }
        stage('Functions'){
          steps{
            echo 'Functions'
            script {
              config['functions'].each{ func ->
                if (func['runtime'] == 'python') {
                  sh "find 'functions/python/${func['name']}/src/' -type f -name \"*.py\" | xargs pylint --disable=E0401 --output-format=parseable --reports=no --exit-zero > 'lint/${func['name']}.pylint.log'"
                }
              }
            }
          }
        }
        stage('Publish Lint') {
          steps {
            recordIssues enabledForFailure: true, tool: pyLint(pattern: 'lint/*.pylint.log')
          }
        }
      }
    }
    stage('Prepare stashes') {
      when {
        branch 'master'
      }
      steps {
        stash name: 'libraries', includes: 'libraries/**'
        script {
          config['functions'].each{ func ->
            stash name: func['name'], includes: "functions/${func['runtime']}/${func['name']}/**"
          }
          config['layers'].each{ layer ->
            stash name: layer['name'], includes: "layers/${layer['runtime']}/${layer['version']}/${layer['name']}/**"
          }
        }
      }
    }
    stage('Build Layers') {
      when {
        branch 'master'
      }
      steps {
        script {
          runParallelLayers items: config['layers']
        }
      }
    }
    stage('Build Lambdas') {
      when {
        branch 'master'
      }
      steps {
        script {
          runParallelLambdas items: config['functions']
        }
      }
    }
  }
  post {
    always {
      deleteDir()
    }
  }
}

def getChanges(path) {
  return sh(
    returnStdout: true,
    script: "git diff --name-only ${env.GIT_COMMIT} ${GIT_PREVIOUS_SUCCESSFUL_COMMIT} | grep ${path} | wc -l"
  ).trim().toInteger()
}

def runParallelLambdas(args) {
  parallel args.items.collectEntries { lambda -> [ "${lambda['name']}": { 
    def buildInfo = readYaml file: "functions/${lambda['runtime']}/${lambda['name']}/build.yaml"
    stage("${lambda['name']}") {
      def changes = getChanges("functions/${lambda['runtime']}/${lambda['name']}")
      buildInfo['libraries'].each { lib ->
        changes += getChanges("libraries/${lambda['runtime']}/${lib}")
      }
      echo "Detected ${changes} changes."
      if (
        (env.BRANCH_NAME == 'master') &&
        (changes > 0)
      ) {
        node("${buildInfo['language']}-${buildInfo['version']}-${buildInfo['architecture']}") {
          try {
            stage('Dependencies') {
              unstash name: 'libraries'
              unstash name: lambda['name']
              buildInfo = readYaml file: "functions/${lambda['runtime']}/${lambda['name']}/build.yaml"
              sh "mkdir -p functions/${lambda['runtime']}/${lambda['name']}/build"
              buildInfo['libraries'].each { lib ->
                sh """
                  cp -r libraries/${lambda['runtime']}/${lib} functions/${lambda['runtime']}/${lambda['name']}/build/
                """
              }
              dir("functions/${lambda['runtime']}/${lambda['name']}") {
                sh "cp -r src/* build/"
                dir('build') {
                  if (fileExists("requirements.txt")) {
                    sh('pip install -r requirements.txt -t .')
                  }
                  buildInfo['libraries'].each { lib ->
                    if (fileExists("${lib}/requirements.txt")) {
                      sh("pip install -r ${lib}/requirements.txt -t .")
                    }
                  }
                }
              }
            }
            stage('Tests') {
              echo ('Future tests go here...')
            }
            stage('Deploy') {
              dir("functions/${lambda['runtime']}/${lambda['name']}") {
                zip zipFile: 'package.zip', dir: 'build', overwrite: true, archive: false
                sh "aws s3 cp ./package.zip s3://bucket/lambda/functions/${lambda['name']}/package.zip"
              }
            }
          } finally {
            buildInfo['extra_directories'].each { extra_dir ->
              dir(extra_dir) {
                deleteDir()
              }
            }
            deleteDir()
          }
        }
      }
      else {
        Utils.markStageSkippedForConditional(STAGE_NAME)
      }
    }
  }]}
}

def runParallelLayers(args) {
  parallel args.items.collectEntries { layer -> [ "${layer['name']}": { 
    echo "layers/${layer['runtime']}/${layer['version']}/${layer['name']}/build.yaml"
    def buildInfo = readYaml file: "layers/${layer['runtime']}/${layer['version']}/${layer['name']}/build.yaml"
    stage("${layer['name']}") {
      def changes = getChanges("layers/${layer['runtime']}/${layer['version']}/${layer['name']}")
      buildInfo['libraries'].each { lib ->
        changes += getChanges("libraries/${layer['runtime']}/${lib}")
      }
      echo "Detected ${changes} changes."
      echo "${buildInfo['name']}: ${buildInfo['build_type']} ${buildInfo['build_type'] == 'zip'}"
      if (
        (env.BRANCH_NAME == 'master')
        && (buildInfo['build_type'] == 'zip')
        && (changes > 0)
      ) {
        node("${buildInfo['language']}-${buildInfo['version']}-${buildInfo['architecture']}") {
          try {
            stage('Dependencies') {
              unstash name: layer['name']
              sh "mkdir -p layers/${layer['runtime']}/${layer['version']}/${layer['name']}/build"
              if (buildInfo['libraries'].size() > 0) {
                unstash name: 'libraries'
              }
              buildInfo['libraries'].each { lib ->
                sh """
                  cp -r libraries/${layer['runtime']}/${lib} "layers/${layer['runtime']}/${layer['version']}/${layer['name']}/build/"
                """
              }
              dir('build') {
                buildInfo['libraries'].each { lib ->
                if (fileExists("${lib}/requirements.txt")) {
                  sh("pip install -r ${lib}/requirements.txt -t python/lib/${layer['runtime']}${layer['version']}/site-packages/")
                }
              }
            }
            stage('Tests') {
              echo ('Future tests go here...')
            }
            stage('Build') {
              dir("layers/${layer['runtime']}/${layer['version']}/${layer['name']}") {
                buildInfo['extra_scripts'].each { extra_script ->
                  sh "bash ${extra_script}"
                }
                if (layer['runtime'] == 'python') {
                  if (fileExists("requirements.txt")) {
                    sh "pip install -r requirements.txt -t build/python/lib/${layer['runtime']}${layer['version']}/site-packages/"
                  }
                }
                }
                buildInfo['extra_directories'].each { extra_dir ->
                  sh "cp -r ${extra_dir}/* build/"
                }
              }
            }
            stage('Deploy') {
              dir("layers/${layer['runtime']}/${layer['version']}/${layer['name']}") {
                zip zipFile: "layer.zip", dir: 'build', overwrite: true, archive: false
                sh "aws s3 cp ./layer.zip s3://bucket/lambda/layers/${layer['name']}.zip"
              }
            }
          } finally {
            buildInfo['extra_directories'].each { extra_dir ->
              dir(extra_dir) {
                deleteDir()
              }
            }
            deleteDir()
          }
        }
      }
      else {
        Utils.markStageSkippedForConditional(STAGE_NAME)
      }
    }
  }]}
}
